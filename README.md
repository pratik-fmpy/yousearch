# YouSearch

Video searching platform

### Overview

- YouSearch is the online search platform built on Django + MySQL
- It contains the APIs to fetch latest videos.
- Videos are sorted  in reverse chronological order of publishing date-time
- We can also search for youtube videos using keywords
- API response is served in the paginated manner

### Highlights

- Server calls the YouTube API continuously in background (async) in a configurable interval (default=10 seconds) for 
  fetching the latest videos for a predefined search query and should store the data of videos in MySQL database
- A GET API is provided which returns the stored video data in a paginated response
sorted in descending order of published datetime.
- A Basic search API is provided to search the stored videos using their title and description.
- The whole project is dockerized
- This project has been made with scalability and optimizations in mind
- Added support for supplying multiple API keys so that if quota is exhaused on one, it automatically uses the next available key.
- A dashboard is provided to view the stored videos with filters and sorting options.
- Optimized the search API, so that it is able to search videos containing partial match for the search query in either video title or description

## Dashboard View

![Dashboard](yousearch_py_api/youtube/static/images/DashboardExample.png)

## API View

![API Example](yousearch_py_api/youtube/static/images/api_example.png)


## API Documentation

#### Search API

- Use this API to get all video details in a paginated response
- Each page contains 10 entries


__REQUEST TYPE__: `GET`

__URL__: `http://127.0.0.1:8000/yt/searchApi`

__RESPONSE BODY TYPE__: `JSON`

__RESPONSE__:
```
{
	"q": "What are Python Programming tools",
	"keywords": ["tools", "Python", "Programming", "are", "What"],
	"order": 0,
	"page": 1,
	"page_start": 1,
	"page_end": 10,
	"total_results": 55,
	"page_results": 10,
	"results": [
		{
			"hits": 5,
			"id": 3,
			"source": "YOUTUBE",
			"videoId": "_uQrJ0TkZlc",
			"publishTime": "2019-02-18T15:00:08",
			"title": "Python Tutorial - Python for Beginners [Full Course]",
			"thumbnail": "https://i.ytimg.com/vi/\\_uQrJ0TkZlc/default.jpg",
			"channelTitle": "Programming with Mosh",
			"description": "Python tutorial - Python for beginners - Go from Zero to Hero with Python (includes machine learning & web development project).",
			"createdAt": "2022-01-09T16:53:08",
			"videoLink": "https://www.youtube.com/watch?v=_uQrJ0TkZlc/",
			"trimmedTitle": "Python Tutorial - Python for Beginners [Full Cours..."
		}
      ]
  }
```

__REQUEST PARAMS__: 

  - `q` = (STRING) Enter any keyword here, it will fetch the videos by matching keyword from either title or description.
  - `order` = (INTEGER) Even or 0 values will return the response in reverse chronological order of publishedDate
  - `page`= (INTEGER) Specify page number here, by default, it has page=1, each page contains 10 response, in order view more, call the same API with new page. Eg. page=2 or page=3 or page=4
  - `titleQ`= (STRING) Enter any keyword here, it will fetch the videos by matching keyword from title only
  - `descQ`= (STRING) Enter any keyword here, it will fetch the videos by matching keyword from description only


__EXAMPLE URLs__:
  - `GET http://127.0.0.1:8000/yt/searchApi`
  - `GET http://127.0.0.1:8000/yt/searchApi?q=python`
  - `GET http://127.0.0.1:8000/yt/searchApi?q=python&order=3`
  - `GET http://127.0.0.1:8000/yt/searchApi?q=python&order=3&page=2`
  - `GET http://127.0.0.1:8000/yt/searchApi?titleQ=pizza`
  - `GET http://127.0.0.1:8000/yt/searchApi?descQ=paneer`


# Project Setup

__Tested this configuration on MacOS__
 - Python Version: `Python 3.6 or higher`
 - Create Virtual environment: `python3 -m venv env`
 - Activate Virtual Environment: `source env/bin/activate`
 - Clone git repository: `git clone https://gitlab.com/shubham_soni_main/yousearch.git`
 - Navigate to project: `cd yousearch/yousearch_py_api/`
 - Create a JSON file named as properties.json: `touch properties.json`
 - Insert these properties into your JSON file, modify the variables to as per your configuration and save
```
{
  "QUERY_TOPIC": "football",
  "SERVICE": "youtube",
  "SERVICE_VERSION": "v3",
  "MAX_RESULT_PACKET": 10,
  "FETCH_YT_PUBLISHED_AFTER": "2018-01-01T10:30:00Z",
  "API_KEYS": {
    "YOUTUBE_API_KEYS": [
      "%@#FN$23p49u2934unsu29u43rn2",
      "bu32b3oy23hbdh282893ydh2n3dh",
      "nhk45u9uj9384dy8ns34238742sn7"
    ]
  },
  "DB_CREDS": {
    "DB_NAME": "yousearch",
    "DB_HOST": "localhost",
    "DB_PORT": 3306,
    "DB_USER": "root",
    "DB_PASSWORD": "mySecurePassword1234"
  },
  "SCHEDULER_CONFIG": {
    "SCHEDULER_MISFIRE_GRACE_TIME": 900,
    "SCHEDULER_POLL_SECONDS": 10,
    "SCHEDULER_MAX_INSTANCE": 1
  }
}
```
 - Make sure MySQL is running on the sepcified port
 - Install the Python requirements: `pip install -r requirements.txt`
 - Start the development server: `python manage.py runserver --noreload`
 - Go to homepage on browser using this link: `http://localhost:8000/yt/home/`


# Project Setup using docker

Run following commands
 - Clone git repository: `git clone https://gitlab.com/shubham_soni_main/yousearch.git`
 - Navigate to project: `cd yousearch/yousearch_py_api/`
 - Create a JSON file named as properties.json: `touch properties.json`
 - Insert properties into your JSON file (see above for reference)
 - `docker-compose build`
 - `docker-compose up -d`
 - Go to homepage on browser using this link: `http://localhost:8000/yt/home/`