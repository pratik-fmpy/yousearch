from apscheduler.schedulers.background import BackgroundScheduler

from yousearch_py_api.settings import SECRETS
from youtube.call_youtube import fetch_youtube_data
from youtube.db_manager.db import YousearchDB


def search_job():
    """
    Scheduler functions that performs following function asynchronously
    1. Create database connection & apply migrations if required
    2. Fetch next page token (if available)
    3. Fetch Youtube data using next page token
    4. Store the Youtube data into database
    5. Store next page token seperately
    6. Close connection
    :return: None
    """
    db_connection = YousearchDB()
    next_page_token = db_connection.fetch_next_page_token()
    next_page_token, items = fetch_youtube_data(next_page_token)
    db_connection.store_next_page_token(next_page_token)
    db_connection.store_video_results(items)
    db_connection.close()


class YoutubeScheduler:
    def __init__(self):
        pass

    def start_scheduler(self):
        """Scheduler initializer function, for fetching youtube data & storing into database"""
        scheduler = BackgroundScheduler(
            job_defaults={'misfire_grace_time': SECRETS['SCHEDULER_CONFIG']['SCHEDULER_MISFIRE_GRACE_TIME']},
            timezone="UTC")
        scheduler.add_job(search_job, 'interval',
                          seconds=SECRETS['SCHEDULER_CONFIG']['SCHEDULER_POLL_SECONDS'],
                          max_instances=SECRETS['SCHEDULER_CONFIG']['SCHEDULER_MAX_INSTANCE'])
        scheduler.start()
