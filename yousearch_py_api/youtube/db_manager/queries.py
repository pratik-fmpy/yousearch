# Read queries
SHOW_DB = "SHOW DATABASES LIKE '{}';"

SEARCH_VID = """SELECT * FROM video WHERE title REGEXP '{}' OR description REGEXP '{}' {} LIMIT {};"""

SEARCH_VID_TITLE = """SELECT * FROM video WHERE title REGEXP '{}' LIMIT {};"""

SEARCH_VID_DESCRIPTION = """SELECT * FROM video WHERE description REGEXP '{}' LIMIT {};"""

COUNT_VID_TITLE = """SELECT COUNT(*) FROM video WHERE title REGEXP '{}';"""

COUNT_VID_DESCRIPTION = """SELECT COUNT(*) FROM video WHERE description REGEXP '{}';"""

SEARCH_ALL_VID = """SELECT * FROM video {} LIMIT {};"""

COUNT_SEARCH_VID = """SELECT COUNT(*) AS 'total' FROM video WHERE title REGEXP '{}' OR description REGEXP '{}';"""

COUNT_ALL_VID = """SELECT COUNT(*) AS 'total' FROM video;"""

FETCH_NEXT_TOKEN = "SELECT nextPageToken FROM list_log LIMIT 1;"

# Write queries
CREATE_DB = 'create database {}'

CREATE_VIDEO_TABLE = """CREATE TABLE IF NOT EXISTS video ( 
                id INT AUTO_INCREMENT NOT NULL UNIQUE KEY,
                source VARCHAR(255) NOT NULL,
                videoId VARCHAR(255) NOT NULL,
                publishTime DATETIME NOT NULL,
                title VARCHAR(255),
                thumbnail VARCHAR(500),
                channelTitle VARCHAR(255),
                description TEXT,
                createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (videoId));"""

CREATE_LIST_LOG_TABLE = """CREATE TABLE IF NOT EXISTS list_log (
              id INT DEFAULT 1,
              nextPageToken VARCHAR(255),
              lastUpdated TIMESTAMP,
              PRIMARY KEY (id));"""

UPDATE_LIST_LOG = """INSERT INTO list_log (id) VALUES (1) ON 
DUPLICATE KEY UPDATE id=1, nextPageToken='{}', lastUpdated=CURRENT_TIMESTAMP;"""

UPDATE_VIDEO = """INSERT INTO video 
(videoId, source, publishTime, title, thumbnail, channelTitle, description) 
VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}') 
ON DUPLICATE KEY UPDATE 
videoId='{}', source='{}', publishTime='{}', title='{}', thumbnail='{}', channelTitle='{}', description='{}';"""
